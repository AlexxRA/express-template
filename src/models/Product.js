const joi = require('@hapi/joi');

const Product = joi.object({
  name: joi.string().required(),
  price: joi.number().required()
});

module.exports = Product;


const express = require('express');
const router = express.Router();
const monk = require('monk');
const { Product } = require('../models');

const db = monk(process.env.MONGO_URL);
const products = db.get('products')

// Get all items
router.get('/', async (req, res, next) => {
  try {
    const items = await products.find({});
    res.json(items);
  } catch (error) {
    next(error);
  }
});

//Get one item
router.get('/:id', async (req, res, next) => {
  try {
    const item = await products.findOne({});
    res.json(item);
  } catch (error) {
    next(error);
  }
});

//Save an item
router.post('/', async (req, res, next) => {
  try {
    const validatedValue = await Product.validateAsync(req.body);
    const inserted = await products.insert(validatedValue);
    res.json(inserted);
  } catch (error) {
    next(error);
  }
});

//Edit an item
router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const validatedValue = await Product.validateAsync(req.body);
    const item = await products.findOne({
      _id: id
    });
    if(!item) return next();
    await products.update({
      _id: id
    },{
      $set: validatedValue
    });
    res.json(validatedValue);
  } catch (error) {
    next(error);
  }
});

//Delete an item
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    await products.remove({ _id: id });
    res.json({
      status: 200
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
